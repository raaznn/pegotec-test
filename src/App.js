import logo from "./logo.svg";
import "./App.css";
import Auth from "./components/Auth";
import Register from "./components/Register";
import UserInfo from "./components/UserInfo";
import { useState } from "react";

function App() {
  const [selectedTask, setSelectedTask] = useState("auth");

  const [info, setInfo] = useState({});
  const [formSubmitted, setformSubmitted] = useState(false);

  // if (selectedTask === "auth") return <Auth />;
  // else
  return (
    <>
      <div className="container-sm mt-5">
        <button
          className="btn btn-lg btn-primary"
          onClick={() => setSelectedTask("auth")}
        >
          Task 1
        </button>
        <button
          className="btn btn-lg btn-primary ms-4"
          onClick={() => setSelectedTask("ui")}
        >
          Task 2
        </button>
      </div>
      {selectedTask === "auth" && <Auth />}
      {selectedTask === "ui" && (
        <div>
          <h1 className="text-center mt-5">Pegotec Task 2 </h1>;
          {formSubmitted ? (
            <UserInfo info={info} />
          ) : (
            <Register setformSubmitted={setformSubmitted} setInfo={setInfo} />
          )}
        </div>
      )}
    </>
  );
}

export default App;
