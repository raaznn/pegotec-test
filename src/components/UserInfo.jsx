import React from "react";

function UserInfo(props) {
  const getAge = (d) => {
    let dob = new Date(d);
    let today = new Date();
    let age = today.getFullYear() - dob.getFullYear();
    return age;
  };
  return (
    <div className="container-sm bg-info">
      {Object.keys(props.info).map((data, ind) => {
        console.log(data);
        if (data === "Date of Birth")
          return (
            <p key={ind} className="d-flex justify-content-between px-4 py-2  ">
              <span className="h4">Age </span>
              <span>{getAge(props.info[data])}</span>
            </p>
          );
        return (
          <p key={ind} className="d-flex justify-content-between px-4 py-2  ">
            <span className="h4">{data} :</span>
            <span>{props.info[data]}</span>
          </p>
        );
      })}
    </div>
  );
}

export default UserInfo;
