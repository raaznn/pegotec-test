import React from "react";
import { useState, useEffect } from "react";

// Company Name
// o Email
// o Address
// o Phone
// o Country - Only Asia (Dropdown form https://restcountries.eu/ or any other third-
// party API)
// o DOB
// o About yourself (Max 300 characters)

function Register(props) {
  const [countryList, setCountryList] = useState([]);
  const [userInfo, setUserInfo] = useState({});

  const getCountries = () => {
    fetch("http://api.worldbank.org/v2/country?format=json")
      .then((res) => res.json())
      .then((data) => {
        setCountryList(
          data[1].map((d) => {
            return { name: d.name, id: d.id };
          })
        );
        // console.log(data);
      })
      .catch((error) => console.log(error));
  };

  const handleChange = (e) => {
    userInfo[e.target.name] = e.target.value;
  };

  const handleSubmit = () => {
    props.setformSubmitted(true);
    props.setInfo(userInfo);
  };
  useEffect(() => {
    getCountries();
  }, []);

  return (
    <div className="container-sm mt-3 mb-5 bg-info p-5">
      <div className="mb-3">
        <label className="form-label">Company Name</label>
        <input
          onChange={handleChange}
          type="text"
          name="Company Name"
          className="form-control"
        />
      </div>
      <div className="mb-3">
        <label className="form-label">Email address</label>
        <input
          onChange={handleChange}
          type="email"
          name="Email"
          className="form-control"
        />
      </div>
      <div className="mb-3">
        <label className="form-label">Address </label>
        <input
          onChange={handleChange}
          type="text"
          name="Address"
          className="form-control"
        />
      </div>
      <div className="mb-3">
        <label className="form-label">Phone number</label>
        <input
          onChange={handleChange}
          type="number"
          name="Phone Number"
          className="form-control"
        />
      </div>
      <div className="mb-3">
        <label className="form-label">Country</label>
        <select onChange={handleChange} name="Country" className="form-select">
          {countryList.map((c) => {
            return (
              <option key={c.id} value={c.name}>
                {c.name}
              </option>
            );
          })}
        </select>
      </div>
      <div className="mb-3">
        <label className="form-label">Date of birth</label>
        <input
          onChange={handleChange}
          type="date"
          name="Date of Birth"
          className="form-control"
        />
      </div>
      <div className="mb-3">
        <label className="form-label" name="">
          About Yourself
        </label>
        <textarea
          onChange={handleChange}
          name="About Yourself"
          maxLength={300}
          className="form-control"
        />
      </div>
      <button onClick={handleSubmit} className="btn btn-primary">
        Submit
      </button>
    </div>
  );
}

export default Register;
