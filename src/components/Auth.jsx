import { useState } from "react";

// const api = "https://api.framework.pegotec.pro/api/auth/";
// email: "team@pegotec.net",
// password:"12345678"

export default function Auth() {
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [loginError, setLoginError] = useState("");
  let [loginStatus, setLoginStatus] = useState(false);

  const login = (email, password) => {
    if (email && password) {
      let data = {
        email,
        password,
      };

      fetch("https://api.framework.pegotec.pro/api/auth/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Success:", data);
          setLoginStatus(true);
          setLoginError("");
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    } else {
      setLoginError("Invalid Email or Password");
    }
  };

  return (
    <div className="mt-4 container-sm text-center">
      <h1>Pegotec Task 1</h1>

      <div className="bg-info p-5 d-flex  flex-column gap-2 align-items-center mt-3">
        <input
          onChange={(e) => setEmail(e.target.value)}
          value={email}
          className="form-control w-50"
          placeholder="Email"
        />
        <input
          className="form-control w-50"
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          placeholder="Password"
        />
        <button
          className="btn-primary btn"
          onClick={() => login(email, password)}
        >
          Login
        </button>

        <div>
          {loginError && <p style={{ color: "red" }}> {loginError} </p>}
        </div>

        <div>
          {loginStatus ? (
            <h2 style={{ color: "green" }}>User is Logged in</h2>
          ) : (
            <h2> User not logged in</h2>
          )}
        </div>
      </div>
    </div>
  );
}
